//función constructora de noticia

function Empleado(nombre, dni, fechaNacimiento, cargo) {

    this.nombre = nombre;
    this.dni = dni;
    this.fechaNacimiento = fechaNacimiento;
    this.cargo = cargo;

}

let empleado1 = new Empleado("Matias", "36086784", "12/3/2000", "cajero");
let empleado2 = new Empleado("José", "22086789", "5/3/1988", "perfumero");

let empleados = [empleado1, empleado2];