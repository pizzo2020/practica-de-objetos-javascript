//función constructora de noticia

function Producto(marca, precio, vencimiento, stock) {

    this.marca = marca;
    this.precio = precio;
    this.vencimiento = vencimiento;
    this.stock = stock;

}

var medicamento1 = new Producto("Tafirol", "$2,50", "29/1/2021", 200);

var medicamento2 = new Producto("Mejoralito", "$10", "30/1/2020", 100);

let medicamentos = [medicamento1, medicamento2];